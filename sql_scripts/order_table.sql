CREATE TABLE `mydb`.`orders` (
  `order_id` INT NOT NULL AUTO_INCREMENT,
  `order_desc` VARCHAR(255) NOT NULL,
  `status` VARCHAR(50) NOT NULL,
  `f_user_id` INT NOT NULL,
  `mechanic_id` INT,
  PRIMARY KEY (`order_id`),
  INDEX `fk_order_userid_idx` (`f_user_id` ASC),
  CONSTRAINT `fk_order_userid`
    FOREIGN KEY (`f_user_id`)
    REFERENCES `mydb`.`user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;