CREATE TABLE `mydb`.`car` (
  `car_id` INT NOT NULL AUTO_INCREMENT,
  `makes` VARCHAR(50) NOT NULL,
  `model` VARCHAR(50) NOT NULL,
  `engine` VARCHAR(50) NOT NULL,
  `mileage` DOUBLE NOT NULL,
  `year` INT NOT NULL,
  `f_user_id` INT NOT NULL,
  PRIMARY KEY (`car_id`),
  INDEX `fk_car_userid_idx` (`f_user_id` ASC),
  CONSTRAINT `fk_car_userid`
    FOREIGN KEY (`f_user_id`)
    REFERENCES `mydb`.`user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;