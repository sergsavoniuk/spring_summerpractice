CREATE TABLE `mydb`.`car_order` (
  `f_car_id` INT NOT NULL,
  `f_order_id` INT NOT NULL,
  PRIMARY KEY (`f_car_id`, `f_order_id`),
  INDEX `fk_carorder_order_idx` (`f_order_id` ASC),
  CONSTRAINT `fk_carorder_car`
    FOREIGN KEY (`f_car_id`)
    REFERENCES `mydb`.`car` (`car_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_carorder_order`
    FOREIGN KEY (`f_order_id`)
    REFERENCES `mydb`.`orders` (`order_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;