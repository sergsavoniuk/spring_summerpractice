CREATE TABLE `mydb`.`role` (
  `role_id` INT NOT NULL AUTO_INCREMENT,
  `role_desc` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `mydb`.`role` (role_desc) VALUES ("ROLE_CLIENT"), ("ROLE_MECHANIC");