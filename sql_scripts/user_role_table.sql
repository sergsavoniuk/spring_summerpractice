CREATE TABLE `mydb`.`user_role` (
  `f_user_id` INT NOT NULL,
  `f_role_id` INT NOT NULL,
  PRIMARY KEY (`f_user_id`, `f_role_id`),
  INDEX `fk_userrole_role_idx` (`f_role_id` ASC),
  CONSTRAINT `fk_userrole_user`
    FOREIGN KEY (`f_user_id`)
    REFERENCES `mydb`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_userrole_role`
    FOREIGN KEY (`f_role_id`)
    REFERENCES `mydb`.`role` (`role_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;