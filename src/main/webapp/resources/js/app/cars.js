cars = [];
var editRowIndex = 0;

function showAllCars() {
    $.ajax( {
        type: 'GET',
        contentType: 'application/json',
        url: '/client/cars/all',
        success: function(data) {
            for (var i = 0; i < data.length; ++i) {
                addRow('cars-table', data[i]);
                cars[i] = data[i];
            }
        }
    });
}

function addCar() {
    var makes = $('#makes').val();
    var model = $('#model').val();
    var engine = $('#engine').val();
    var mileage = $('#mileage').val();
    var year = $('#year').val();
    var order = $('#order').val();

    $.ajax( {
        type: 'POST',
        contentType: 'application/json',
        url: '/client/cars/save',
        data: JSON.stringify({'makes':makes, 'model':model, 'engine':engine, 'mileage':mileage, 'year':year}),
        dataType: 'json',
        success: function(data) {
            addRow('cars-table', data);
            clearInputs();
            clearSpan();
            cars.push(data);
        },
        error: function(error) {
            showErrors(error);
        }
    });
}

function showErrors(error) {
    clearSpan();
    for (var i = 0; i < error.responseJSON.errors.length; ++i) {
        var arr = error.responseJSON.errors[i].split(',');
        var field = arr[0].trim();
        var message = arr[1].trim();
        addErrorMessageToField(field, message);
    }
}

function addErrorMessageToField(field, message) {
    switch (field) {
        case 'makes':
            $('#makes-err').text('*' + message);
            break;
        case 'model':
            $('#model-err').text('*' + message);
            break;
        case 'engine':
            $('#engine-err').text('*' + message);
            break;
        case 'mileage':
            $('#mileage-err').text('*' + message);
            break;
        case 'year':
            $('#year-err').text('*' + message);
            break;
        default:
            alert(message);
            break;
    }
}

function addCol(row, cellNum, value) {
    if (typeof value === 'string' || typeof value === 'number') {
        row.insertCell(cellNum).innerHTML = value;
    } else {
        row.insertCell(cellNum).appendChild(value);
    }
}

function addRow(tableId, data) {
    var table = document.getElementById(tableId);
    var row = table.insertRow(-1);
    row.className = "text-center";

    addCol(row, 0, data.id);
    addCol(row, 1, data.makes);
    addCol(row, 2, data.model);
    addCol(row, 3, data.engine);
    addCol(row, 4, data.mileage);
    addCol(row, 5, data.year);

    var buttonEdit = document.createElement('button');
    buttonEdit.className = 'btn btn-primary btn-sm';
    buttonEdit.innerHTML = 'Edit';

    addCol(row, 6, buttonEdit);

    buttonEdit.addEventListener("click", function() {
        editRowIndex = getEditedRowIndex(this);
        setEditedData(editRowIndex);
        disableAddButton();
    });

    var buttonDelete = document.createElement('button');
    buttonDelete.className = 'btn btn-danger btn-sm';
    buttonDelete.innerHTML = 'Delete';

    addCol(row, 7, buttonDelete);

    buttonDelete.addEventListener("click", function() {
        var index = getDeletedRowIndex(this);
        var carId = cars[index - 1].id;

        $.ajax( {
            type: 'POST',
            contentType: 'application/json',
            url: '/client/cars/delete/' + carId,
            data: JSON.stringify(carId),
            dataType: 'json',
            success: function(data) {
                deleteRow(tableId, index);
                cars.splice(index - 1, 1);
            }
        });
    });

    var buttonAdd = document.createElement('button');
    buttonAdd.className = 'btn btn-danger btn-sm';
    buttonAdd.innerHTML = 'Add';

    addCol(row, 8, buttonAdd);

    buttonAdd.addEventListener("click", function() {
        var index = getSelectedRowIndex(this);
        location.href = "/client/cars/"+ cars[index - 1].id + "/order/add";
    });

    $('#btn-save').click(function() {
        var json = getNewData();

        $.ajax( {
            type: 'POST',
            contentType: 'application/json',
            url: '/client/cars/edit',
            data: json,
            dataType: 'json',
            success: function(data) {
                cars.splice(editRowIndex - 1, 1);
                cars[editRowIndex - 1] = data;
                editRow(tableId, editRowIndex);
                clearInputs();
                clearSpan();
                enabledAddButton();
            },
            error: function(error) {
                showErrors(error);
            }
        });
    });
}

function getDeletedRowIndex($this) {
    return $this.parentNode.parentNode.rowIndex;
}

function getSelectedRowIndex($this) {
    return $this.parentNode.parentNode.rowIndex;
}

function getEditedRowIndex($this) {
    editRowIndex = $this.parentNode.parentNode.rowIndex;
    return editRowIndex;
}

function deleteRow(tableId, index) {
    document.getElementById(tableId).deleteRow(index);
}

function setEditedData(index) {
    $('#carId').val(cars[index - 1].id);
    $('#makes').val(cars[index - 1].makes);
    $('#model').val(cars[index - 1].model);
    $('#engine').val(cars[index - 1].engine);
    $('#mileage').val(cars[index - 1].mileage);
    $('#year').val(cars[index - 1].year);
}

function disableAddButton() {
    $('#btn-add').attr('disabled',true);
    $('#btn-save').removeAttr('disabled');
    $('#btn-cancel').removeAttr('disabled');
}

function enabledAddButton() {
    $('#btn-add').removeAttr('disabled');
    $('#btn-save').attr('disabled', true);
    $('#btn-cancel').attr('disabled', true);
}

function getNewData() {
    var id = $('#carId').val();
    var makes = $('#makes').val();
    var model = $('#model').val();
    var engine = $('#engine').val();
    var mileage = $('#mileage').val();
    var year = $('#year').val();
    return JSON.stringify({'id': id, 'makes': makes, 'model': model, 'engine': engine, 'mileage':mileage, 'year': year});
}

function editRow(tableId, index) {
    var table = document.getElementById(tableId);
    var newRow = table.insertRow(index);
    newRow.className = "text-center";
    editCol(newRow, 0, cars[index - 1].id);
    editCol(newRow, 1, cars[index - 1].makes);
    editCol(newRow, 2, cars[index - 1].model);
    editCol(newRow, 3, cars[index - 1].engine);
    editCol(newRow, 4, cars[index - 1].mileage);
    editCol(newRow, 5, cars[index - 1].year);

    var buttonEdit = document.createElement('button');
    buttonEdit.className = 'btn btn-primary btn-sm';
    buttonEdit.innerHTML = 'Edit';

    editCol(newRow, 6, buttonEdit);

    buttonEdit.addEventListener("click", function() {
        editRowIndex = getEditedRowIndex(this);
        setEditedData(editRowIndex);
        disableAddButton();
    });

    var buttonDelete = document.createElement('button');
    buttonDelete.className = 'btn btn-danger btn-sm';
    buttonDelete.innerHTML = 'Delete';

    editCol(newRow, 7, buttonDelete);

    buttonDelete.addEventListener("click", function() {
        var index = getDeletedRowIndex(this);
        var carId = cars[index - 1].id;

        $.ajax( {
            type: 'POST',
            contentType: 'application/json',
            url: '/client/cars/delete/' + carId,
            data: JSON.stringify(carId),
            dataType: 'json',
            success: function(data) {
                deleteRow(tableId, index);
                cars.splice(index - 1, 1);
            }
        });
    });

    var buttonAdd = document.createElement('button');
    buttonAdd.className = 'btn btn-primary btn-sm';
    buttonAdd.innerHTML = 'Add';

    addCol(newRow, 8, buttonAdd);

    table.deleteRow(index + 1);
}

function editCol(row, colIndex, value) {
    if (typeof value === 'string' || typeof value == 'number') {
        row.insertCell(colIndex).innerHTML = value;
    } else {
        row.insertCell(colIndex).appendChild(value);
    }
}

function clearInputs() {
    $('#carId').val('');
    $('#makes').val('');
    $('#model').val('');
    $('#engine').val('');
    $('#mileage').val('');
    $('#year').val('');
}

function clearSpan() {
    $('#makes-err').text('');
    $('#model-err').text('');
    $('#engine-err').text('');
    $('#mileage-err').text('');
    $('#year-err').text('');
}