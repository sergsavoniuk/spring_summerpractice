function signUpUser() {
    var username = $('#username').val();
    var password = $('#password').val();
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var phoneNumber = $('#phoneNumber').val();

    $.ajax( {
        type: 'POST',
        contentType: 'application/json',
        url: '/signup-perform',
        data: JSON.stringify({'username':username, 'password':password, 'firstName':firstName, 'lastName':lastName, 'phoneNumber':phoneNumber}),
        dataType: 'json',
        success: function(data, response) {
            clearSpan();
            if (response == 'success') {
                alert('User with login ' + data.username + ' successfully registered.\n' +
                    'Click OK and log in using your username');
                location.href = '/login';
            } else {
                $('#error').text('Unknown error!');
            }
        },
        error: function(error) {
            showErrors(error);
        }
    });
}

function showErrors(error) {
    clearSpan();
    var field, message;
    for (var i = 0; i < error.responseJSON.errors.length; ++i) {
        var arr = error.responseJSON.errors[i].split(',');
        if (arr.length > 1) {
            field = arr[0].trim();
            message = arr[1].trim();
        } else {
            message = arr[0];
            field = '';
        }
        addErrorMessageToField(field, message);
    }
}

function addErrorMessageToField(field, message) {
    switch (field) {
        case 'username':
            $('#username-err').text(message);
            break;
        case 'password':
            $('#password-err').text(message);
            break;
        case 'firstName':
            $('#first-name-err').text(message);
            break;
        case 'lastName':
            $('#last-name-err').text(message);
            break;
        case 'phoneNumber':
            $('#phone-number-err').text(message);
            break;
        default:
            $('#error').text(message);
            break;
    }
}

function clearSpan() {
    $('#username-err').text('');
    $('#password-err').text('');
    $('#first-name-err').text('');
    $('#last-name-err').text('');
    $('#phone-number-err').text('');
}