function addOrderToCar() {
    var orderDesc = $('#order').val();

    $.ajax({
            url: '/client/cars/' + $('#carId').val() + '/order/add/perform',
            type: 'POST',
            contentType : 'application/json',
            dataType: 'json',
            data: JSON.stringify({
                'orderDesc': orderDesc
            }),
            success: function(data){
                location.href = "/client/homepage";
            },
            error: function (data){
                alert(data.responseText);
            }
        }
    );
}