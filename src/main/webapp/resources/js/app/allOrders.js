orders = [];

function showAllOrders() {
    $.ajax( {
        type: 'GET',
        contentType: 'application/json',
        url: '/mechanic/orders/all',
        success: function(data) {
            for (var i = 0; i < data.length; ++i) {
                addRow('orders-table', data[i]);
                orders[i] = data[i];
            }
        }
    });
}

function addCol(row, cellNum, value) {
    if (typeof value === 'string' || typeof value === 'number') {
        row.insertCell(cellNum).innerHTML = value;
    } else {
        row.insertCell(cellNum).appendChild(value);
    }
}

function addRow(tableId, data) {
    var table = document.getElementById(tableId);
    var row = table.insertRow(-1);
    row.className = "text-center";

    addCol(row, 0, data.id);
    addCol(row, 1, data.carOut.id);
    addCol(row, 2, data.carOut.makes);
    addCol(row, 3, data.carOut.model);
    addCol(row, 4, data.carOut.engine);
    addCol(row, 5, data.carOut.mileage);
    addCol(row, 6, data.carOut.year);
    addCol(row, 7, data.orderDesc);
    addCol(row, 8, data.status);

    var buttonTake = document.createElement('button');

    if (data.status == 'Performed' || data.status == 'Done') {
        buttonTake.className = 'btn btn-primary btn-sm disabled';
    } else {
        buttonTake.className = 'btn btn-primary btn-sm';
    }
    buttonTake.innerHTML = 'Take order';

    addCol(row, 9, buttonTake);

    buttonTake.addEventListener("click", function() {
        var index = getSelectedRowIndex(this);
        var orderId = orders[index - 1].id;

        $.ajax( {
            type: 'POST',
            contentType: 'application/json',
            url: '/mechanic/orders/save',
            data: JSON.stringify(orderId),
            dataType: 'json',
            success: function(data) {
                buttonTake.className = 'btn btn-primary btn-sm disabled';
                alert('Order succesfully done')
            },
            error: function(error) {
                alert('error');
            }
        });
    });
}

function getSelectedRowIndex($this) {
    return $this.parentNode.parentNode.rowIndex;
}