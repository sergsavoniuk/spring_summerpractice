<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script type="text/javascript" src="<c:url value="/resources/js/lib/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/lib/jquery-3.0.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/app/orderForCar.js"/>"></script>
</head>
<body>

<div class="container">
    <h2 align="center">Add order</h2>
    <form class="form-horizontal">

        <div class="form-group">
            <div>
                <input type=text hidden="hidden" id="carId" value=${car.id}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Makes</label>
            <div class="col-sm-7">
                <input type=text class="form-control" id="makes" disabled="disabled" value=${car.makes}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Model</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="model" disabled="disabled" value=${car.model}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Engine</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="engine" disabled="disabled" value=${car.engine}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Mileage (km)</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="mileage" disabled="disabled" value=${car.mileage}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Year</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="year" disabled="disabled" value=${car.year}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Order</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5" id="order"></textarea>
            </div>
            <div>
                <span style="color:red" id="order-err"></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-3">
                <button type="button" id="btn-save"
                        class="btn btn-success">Save</button>
                <button type="button" id="btn-cancel"
                        class="btn btn-default">Cancel</button>
            </div>
        </div>
    </form>
</div>

<script>
    $('#btn-cancel').click(function() {
        location.href = "/client/cars";
    });

    $('#btn-save').click(function() {
        addOrderToCar();
    });
</script>

</body>
</html>