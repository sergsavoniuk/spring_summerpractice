<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/login.css"/>">
    <script type="text/javascript" src="<c:url value="/resources/js/lib/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/lib/jquery-3.0.0.min.js"/>"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-4 top-padding">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title" align="center">Authorization</h3>
                </div>
                <div class="panel-body">
                    <c:url value="/j_spring_security_check" var="loginUrl"/>
                    <form action="${loginUrl}" method="post">

                        <c:if test="${param.error != null}">
                            <div class="alert alert-danger">
                                <p>Invalid username or password.</p>
                            </div>
                        </c:if>
                        <c:if test="${param.logout != null}">
                            <div class="alert alert-success">
                                <p>You have been logged out successfully.</p>
                            </div>
                        </c:if>

                        <div class="form-group">
                            <span style="color:red" id="usernameErr"></span>
                            <input type="text" name="j_username" class="form-control" placeholder="Username">
                        </div>

                        <div class="form-group">
                            <span style="color:red" id="passwordErr"></span>
                            <input type="password" name="j_password" class="form-control" placeholder="Password">
                        </div>

             <%--           <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />--%>

                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success btn-md">SIGN IN</button>
                            <label>Not account yet? <a href="<c:url value="/signup"/>">Create an account</a></label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>