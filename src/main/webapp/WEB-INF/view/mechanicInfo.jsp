<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/userHomePage.css"/>">
    <script type="text/javascript" src="<c:url value="/resources/js/lib/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/lib/jquery-3.0.0.min.js"/>"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <h2 align="center">Dear ${pageContext.request.userPrincipal.name}, welcome to Mechanic Page</h2>
        </c:if>
        <div class="col-md-5 toppad pull-right col-md-offset-3">
            <A href="<c:url value="/"/>" >Home</A>
            <A href="<c:url value="/logout"/>" >Logout</A>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title" align="center">User Profile</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="<c:url value="/resources/image/cat.jpg"/>" class="img-circle img-responsive"> </div>
                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td><strong>User level</strong></td>
                                    <td><strong>Mechanic</strong></td>
                                </tr>
                                <tr>
                                    <td>Username</td>
                                    <td>${user.username}</td>
                                </tr>
                                <tr>
                                    <td>First Name</td>
                                    <td>${user.firstName}</td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td>${user.lastName}</td>
                                </tr>
                                <tr>
                                    <td>Phone Number</td>
                                    <td>${user.phoneNumber}</td>
                                </tr>
                                </tbody>
                            </table>

                            <a href="<c:url value="/mechanic/orders"/>" class="btn btn-primary">All orders</a>
                            <a href="<c:url value="/mechanic/orders/my"/>" class="btn btn-primary">My orders</a>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>