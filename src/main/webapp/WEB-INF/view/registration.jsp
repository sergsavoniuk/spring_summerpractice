<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/registration.css"/>">
    <script src="<c:url value="/resources/js/lib/jquery-3.0.0.min.js"/>"></script>
    <script src="<c:url value="/resources/js/lib/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/app/registration.js"/>"></script>
</head>
<body>

<br>
<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-4 top-padding">
            <h4><span class="label label-danger" id="error"></span></h4>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title" align="center">Registration</h3>
                </div>
                <div class="panel-body">
                    <form id="loginForm" role="form">

                        <div class="form-group">
                            <span style="color:red" id="username-err"></span>
                            <input type="text" id="username" class="form-control" placeholder="Username">
                        </div>

                        <div class="form-group">
                            <span style="color:red" id="password-err"></span>
                            <input type="password" id="password" class="form-control" placeholder="Password">
                        </div>

                        <div class="form-group">
                            <span style="color:red" id="first-name-err"></span>
                            <input type="text" id="firstName" class="form-control" placeholder="First Name">
                        </div>

                        <div class="form-group">
                            <span style="color:red" id="last-name-err"></span>
                            <input type="text" id="lastName" class="form-control" placeholder="Last Name">
                        </div>

                        <div class="form-group">
                            <span style="color:red" id="phone-number-err"></span>
                            <input type="text" id="phoneNumber" class="form-control" placeholder="Phone Number">
                        </div>

                        <button type="button" id="signUp" class="btn btn-block btn-success">SIGN UP</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        $('#signUp').click(function(e) {
            signUpUser();
        })
    });

</script>

</body>
</html>