<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/home.css"/>">
</head>
<body>
    <div class="container">
        <h1>Welcome to our website</h1>
        <h2>
            Please <a href="<c:url value="/login"/>">sign in</a> your account or
            <a href="<c:url value="/signup"/>">create</a> new account
        </h2>
    </div>
</body>
</html>
