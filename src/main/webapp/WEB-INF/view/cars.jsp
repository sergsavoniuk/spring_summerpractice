<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script type="text/javascript" src="<c:url value="/resources/js/lib/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/lib/jquery-3.0.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/app/cars.js"/>"></script>
</head>
<body>

<div class="container">
    <div>
        <A href="<c:url value="/client/homepage"/>" >Home Page</A>
    </div>
    <h2 align="center">Add/Edit car</h2>
    <form class="form-horizontal">
        <div class="form-group">
            <div>
                <input type=text id="carId" hidden="hidden">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Makes</label>
            <div class="col-sm-7">
                <input type=text class="form-control" id="makes">
            </div>
            <div>
                <span style="color:red" id="makes-err"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Model</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="model">
            </div>
            <div>
                <span style="color:red" id="model-err"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Engine</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="engine">
            </div>
            <div>
                <span style="color:red" id="engine-err"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Mileage (km)</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="mileage">
            </div>
            <div>
                <span style="color:red" id="mileage-err"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Year</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="year">
            </div>
            <div>
                <span style="color:red" id="year-err"></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-3">
                <button type="button" id="btn-add"
                        class="btn btn-primary">Add</button>
                <button type="button" id="btn-save"
                        class="btn btn-primary">Save</button>
                <button type="button" id="btn-cancel"
                        class="btn btn-primary">Cancel</button>
            </div>
        </div>
    </form>
</div>

<div class="container">
    <h2 align="center">List of cars</h2>
    <table class="table table-bordered" id="cars-table">
        <thead>
        <tr class="text-center">
            <th class="text-center">ID</th>
            <th class="text-center">Makes</th>
            <th class="text-center">Model</th>
            <th class="text-center">Engine</th>
            <th class="text-center">Mileage (km)</th>
            <th class="text-center">Year</th>
            <th class="text-center" colspan="2">Action</th>
            <th class="text-center">Order</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script>
    $('#btn-add').click(function() {
        addCar();
    });

    $('#btn-cancel').click(function() {
        clearInputs();
        enabledAddButton();
    });

    window.onload = function() {
        enabledAddButton();
        showAllCars();
    };
</script>

</body>
</html>