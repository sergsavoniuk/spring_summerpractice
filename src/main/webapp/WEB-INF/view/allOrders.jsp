<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script type="text/javascript" src="<c:url value="/resources/js/lib/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/lib/jquery-3.0.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/app/allOrders.js"/>"></script>
</head>
<body>

<div class="container">
    <div>
        <A href="<c:url value="/mechanic/homepage"/>" >Home Page</A>
    </div>
    <h2 align="center">List of orders</h2>
    <table class="table table-bordered" id="orders-table">
        <thead>
        <tr class="text-center">
            <th class="text-center">Order ID</th>
            <th class="text-center">Car ID</th>
            <th class="text-center">Makes</th>
            <th class="text-center">Model</th>
            <th class="text-center">Engine</th>
            <th class="text-center">Mileage (km)</th>
            <th class="text-center">Year</th>
            <th class="text-center">Order</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script>
    window.onload = function() {
        showAllOrders();
    };
</script>

</body>
</html>