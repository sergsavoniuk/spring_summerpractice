<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script type="text/javascript" src="<c:url value="/resources/js/lib/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/lib/jquery-3.0.0.min.js"/>"></script>
</head>
<body>

<div class="container">
    <div>
        <A href="<c:url value="/client/homepage"/>" >Home Page</A>
    </div>
    <h2 align="center">Your orders</h2>
    <table class="table table-bordered" id="orders-table">
        <thead>
        <tr class="text-center">
            <th class="text-center">Order ID</th>
            <th class="text-center">Car ID</th>
            <th class="text-center">Makes</th>
            <th class="text-center">Model</th>
            <th class="text-center">Engine</th>
            <th class="text-center">Mileage (km)</th>
            <th class="text-center">Year</th>
            <th class="text-center">Order</th>
            <th class="text-center">Status</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${orders}" var="order">
            <tr>
                <td class="text-center"><c:out value="${order.id}"/></td>
                <td class="text-center"><c:out value="${order.carOut.id}"/></td>
                <td class="text-center"><c:out value="${order.carOut.makes}"/></td>
                <td class="text-center"><c:out value="${order.carOut.model}"/></td>
                <td class="text-center"><c:out value="${order.carOut.engine}"/></td>
                <td class="text-center"><c:out value="${order.carOut.mileage}"/></td>
                <td class="text-center"><c:out value="${order.carOut.year}"/></td>
                <td class="text-center"><c:out value="${order.orderDesc}"/></td>
                <td class="text-center"><c:out value="${order.status}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>