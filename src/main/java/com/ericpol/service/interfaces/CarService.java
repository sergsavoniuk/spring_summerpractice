package com.ericpol.service.interfaces;

import com.ericpol.dto.CarIn;
import com.ericpol.dto.CarOut;
import com.ericpol.model.Car;

import java.util.List;

public interface CarService {
    CarOut save(CarIn carIn);
    void delete(Long id);
    CarOut update(CarOut carOut);
    Car findById(Long id);
    List<CarOut> findAllUserCars(String username);
    List<CarOut> findAll();
}