package com.ericpol.service.interfaces;

import com.ericpol.dto.OrderIn;
import com.ericpol.dto.OrderOut;
import com.ericpol.model.Order;

import java.util.List;

public interface OrderService {
    OrderOut save(OrderIn orderIn, Long carId);
    OrderOut save(Long orderId);
    Order findById(Long id);
    List<OrderOut> findAllUserOrders(String username);
    List<OrderOut> findAllMechanicOrders(String username);
    List<OrderOut> findAllOrders();
    List<Order> findAll();
}