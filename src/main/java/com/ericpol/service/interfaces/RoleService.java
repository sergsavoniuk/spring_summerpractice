package com.ericpol.service.interfaces;

import com.ericpol.model.Role;

public interface RoleService {
    Role findByRoleDesc(String roleDesc);
}
