package com.ericpol.service.interfaces;

import com.ericpol.dto.UserTo;
import com.ericpol.model.User;

import java.util.List;

public interface UserService {
    User save(UserTo userTo);
    void delete(Long id);
    User findById(Long id);
    User findByUsername(String username);
    List<User> findAll();
}