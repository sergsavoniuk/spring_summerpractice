package com.ericpol.service.impl;

import com.ericpol.dto.CarIn;
import com.ericpol.dto.CarOut;
import com.ericpol.dto.FactoryTo;
import com.ericpol.exception.EntityNotFound;
import com.ericpol.model.Car;
import com.ericpol.model.User;
import com.ericpol.repository.CarRepository;
import com.ericpol.repository.UserRepository;
import com.ericpol.service.interfaces.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Qualifier("carRepository")
    @Autowired
    private CarRepository carRepository;

    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FactoryTo factoryTo;

    @Transactional
    @Override
    public CarOut save(CarIn carIn) {
        Car car = factoryTo.getCar(carIn);
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.findByUsername(userDetails.getUsername());
        car.setUser(user);
        car = carRepository.save(car);
        return factoryTo.getCar(car);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Car car = carRepository.findOne(id);
        if (car == null) {
            throw new EntityNotFound("Car with id = " + id + " not found");
        } else  {
            carRepository.deleteById(id);
        }
    }

    @Transactional
    @Override
    public CarOut update(CarOut carOut) {
        Car car = carRepository.findOne(carOut.getId());
        if (car == null) {
            throw new EntityNotFound("Car with id = " + carOut.getId() + " not found");
        } else {
            car.setMakes(carOut.getMakes());
            car.setModel(carOut.getModel());
            car.setEngine(carOut.getEngine());
            car.setMileage(carOut.getMileage());
            car.setYear(carOut.getYear());
            car = carRepository.save(car);
        }
        return factoryTo.getCar(car);
    }

    @Transactional(readOnly = true)
    @Override
    public Car findById(Long id) {
        return carRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<CarOut> findAllUserCars(String username) {
        List<Car> cars = carRepository.findAllUserCars(username);
        List<CarOut> carsOut = new ArrayList<>();
        for (Car car : cars) {
            carsOut.add(factoryTo.getCar(car));
        }
        return carsOut;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CarOut> findAll() {
        List<Car> cars = (List<Car>) carRepository.findAll();
        List<CarOut> carsOut = new ArrayList<>();
        for (Car car : cars) {
            carsOut.add(factoryTo.getCar(car));
        }
        return carsOut;
    }
}