package com.ericpol.service.impl;

import com.ericpol.dto.FactoryTo;
import com.ericpol.dto.UserTo;
import com.ericpol.exception.EntityAlreadyExist;
import com.ericpol.exception.EntityNotFound;
import com.ericpol.model.Role;
import com.ericpol.model.User;
import com.ericpol.repository.RoleRepository;
import com.ericpol.repository.UserRepository;
import com.ericpol.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;

    @Qualifier("roleRepository")
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private FactoryTo factoryTo;

    @Transactional
    @Override
    public User save(UserTo userTo) {
        Role role;
        User user;
        if (userExist(userTo.getUsername())) {
            throw new EntityAlreadyExist("User with login " + userTo.getUsername() + " already exist");
        } else {
            if (userTo.getPassword().equals("mechanic")) {
                role = roleRepository.findByRoleDesc("ROLE_MECHANIC");
            } else {
                role = roleRepository.findByRoleDesc("ROLE_CLIENT");
            }
            user = factoryTo.getUser(userTo);
            List<Role> roles = new ArrayList<>();
            roles.add(role);
            user.setRoles(roles);
        }
        return userRepository.save(user);
    }

    @Transactional
    @Override
    public void delete(Long id){
        User user = findById(id);
        if (user == null) {
            throw new EntityNotFound("User with id = " + id + " not found");
        } else {
            userRepository.delete(id);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public User findById(Long id) {
        return userRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional(readOnly = true)
    @Override
    public List<User> findAll() {
        return (List<User>)userRepository.findAll();
    }

    private boolean userExist(String username) {
        return userRepository.findByUsername(username) != null;
    }
}