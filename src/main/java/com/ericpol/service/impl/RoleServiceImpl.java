package com.ericpol.service.impl;

import com.ericpol.model.Role;
import com.ericpol.repository.RoleRepository;
import com.ericpol.service.interfaces.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleServiceImpl implements RoleService {

    @Qualifier("roleRepository")
    @Autowired
    private RoleRepository roleRepository;

    @Transactional(readOnly = true)
    @Override
    public Role findByRoleDesc(String roleDesc) {
        return roleRepository.findByRoleDesc(roleDesc);
    }
}