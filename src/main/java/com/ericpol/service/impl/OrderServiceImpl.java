package com.ericpol.service.impl;

import com.ericpol.dto.FactoryTo;
import com.ericpol.dto.OrderIn;
import com.ericpol.dto.OrderOut;
import com.ericpol.model.Order;
import com.ericpol.model.User;
import com.ericpol.repository.CarRepository;
import com.ericpol.repository.OrderRepository;
import com.ericpol.repository.UserRepository;
import com.ericpol.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Qualifier("orderRepository")
    @Autowired
    private OrderRepository orderRepository;

    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;

    @Qualifier("carRepository")
    @Autowired
    private CarRepository carRepository;

    @Autowired
    private FactoryTo factoryTo;

    @Transactional
    @Override
    public OrderOut save(OrderIn orderIn, Long carId) {
        Order order = factoryTo.getOrder(orderIn);
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.findByUsername(userDetails.getUsername());
        order.setUser(user);
        order.setCars(Collections.singletonList(carRepository.findOne(carId)));
        order = orderRepository.save(order);
        return factoryTo.getOrder(order);
    }

    @Transactional
    @Override
    public OrderOut save(Long orderId) {
        Order order = orderRepository.findOne(orderId);
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.findByUsername(userDetails.getUsername());
        order.setMechanicId(user.getUserId());
        if (order.getStatus().equals("Processing")) {
            order.setStatus("Performed");
        } else {
            order.setStatus("Done");
        }
        order = orderRepository.save(order);
        return factoryTo.getOrder(order);
    }

    @Transactional(readOnly = true)
    @Override
    public Order findById(Long id) {
        return orderRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<OrderOut> findAllUserOrders(String username) {
        List<Order> orders = orderRepository.findAllUserOrders(username);
        List<OrderOut> ordersOut = new ArrayList<>();
        for (Order order : orders) {
            ordersOut.add(factoryTo.getOrder(order, factoryTo.getCar(order.getCars().get(0))));
        }
        return ordersOut;
    }

    @Transactional(readOnly = true)
    @Override
    public List<OrderOut> findAllMechanicOrders(String username) {
        User user = userRepository.findByUsername(username);
        List<Order> orders = orderRepository.findAllMechanicOrders(user.getUserId());
        List<OrderOut> ordersOut = new ArrayList<>();
        for (Order order : orders) {
            ordersOut.add(factoryTo.getOrder(order, factoryTo.getCar(order.getCars().get(0))));
        }
        return ordersOut;
    }

    @Transactional(readOnly = true)
    @Override
    public List<OrderOut> findAllOrders() {
        List<Order> orders = orderRepository.findAllOrders();
        List<OrderOut> ordersOut = new ArrayList<>();
        for (Order order : orders) {
            ordersOut.add(factoryTo.getOrder(order, factoryTo.getCar(order.getCars().get(0))));
        }
        return ordersOut;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Order> findAll() {
        return (List<Order>)orderRepository.findAll();
    }
}