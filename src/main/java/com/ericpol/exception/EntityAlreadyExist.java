package com.ericpol.exception;

public class EntityAlreadyExist extends RuntimeException {

    public EntityAlreadyExist() {

    }

    public EntityAlreadyExist(String message) {
        super(message);
    }
}
