package com.ericpol.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id", nullable = false)
    private Long orderId;

    @NotEmpty
    @Column(name = "order_desc", nullable = false)
    private String orderDesc;

    @NotEmpty
    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "mechanic_id", nullable = true)
    private Long mechanicId;

    @ManyToOne
    @JoinColumn(name = "f_user_id")
    private User userOrder;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "car_order", joinColumns = @JoinColumn(name = "f_order_id"),
            inverseJoinColumns = @JoinColumn(name = "f_car_id"))
    private List<Car> cars = new ArrayList<>();

    public Order() {
    }

    public Order(String orderDesc, String status) {
        this.orderDesc = orderDesc;
        this.status = status;
    }

    public Order(String orderDesc, String status, Long mechanicId) {
        this.orderDesc = orderDesc;
        this.status = status;
        this.mechanicId = mechanicId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getMechanicId() {
        return mechanicId;
    }

    public void setMechanicId(Long mechanicId) {
        this.mechanicId = mechanicId;
    }

    public User getUser() {
        return userOrder;
    }

    public void setUser(User user) {
        this.userOrder = user;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}