package com.ericpol.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id", nullable = false)
    private Long carId;

    @Column(name = "makes", nullable = false)
    private String makes;

    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "engine", nullable = false)
    private String engine;

    @Column(name = "mileage", nullable = false)
    private Double mileage;

    @Column(name = "year", nullable = false)
    private Integer year;

    @ManyToOne
    @JoinColumn(name = "f_user_id")
    private User userCar;

    @ManyToMany(mappedBy = "cars", cascade = CascadeType.ALL)
    private List<Order> orders = new ArrayList<>();

    public Car() {
    }

    public Car(String makes, String model, String engine, Double mileage, Integer year) {
        this.makes = makes;
        this.model = model;
        this.engine = engine;
        this.mileage = mileage;
        this.year = year;
    }

    public Long getId() {
        return carId;
    }

    public void setId(Long carId) {
        this.carId = carId;
    }

    public String getMakes() {
        return makes;
    }

    public void setMakes(String makes) {
        this.makes = makes;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public User getUser() {
        return userCar;
    }

    public void setUser(User user) {
        this.userCar = user;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carId=" + carId +
                ", makes='" + makes + '\'' +
                ", model='" + model + '\'' +
                ", engine='" + engine + '\'' +
                ", mileage=" + mileage +
                ", year=" + year +
                '}';
    }
}