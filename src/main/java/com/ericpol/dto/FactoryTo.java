package com.ericpol.dto;

import com.ericpol.model.Car;
import com.ericpol.model.Order;
import com.ericpol.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class FactoryTo {

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User getUser(UserTo userTo) {
        User user = new User();
        user.setUsername(userTo.getUsername());
        user.setPassword(passwordEncoder.encode(userTo.getPassword()));
        user.setFirstName(userTo.getFirstName());
        user.setLastName(userTo.getLastName());
        user.setPhoneNumber(userTo.getPhoneNumber());
        return user;
    }

    public Car getCar(CarIn carIn) {
        Car car = new Car();
        car.setMakes(carIn.getMakes());
        car.setModel(carIn.getModel());
        car.setEngine(carIn.getEngine());
        car.setMileage(carIn.getMileage());
        car.setYear(carIn.getYear());
        return car;
    }

    public CarOut getCar(Car car) {
        CarOut carOut = new CarOut();
        carOut.setId(car.getId());
        carOut.setMakes(car.getMakes());
        carOut.setModel(car.getModel());
        carOut.setEngine(car.getEngine());
        carOut.setMileage(car.getMileage());
        carOut.setYear(car.getYear());
        return carOut;
    }

    public Order getOrder(OrderIn orderIn) {
        Order order = new Order();
        order.setOrderDesc(orderIn.getOrderDesc());
        order.setStatus("Processing");
        return order;
    }

    public OrderOut getOrder(Order order) {
        OrderOut orderOut = new OrderOut();
        orderOut.setId(order.getOrderId());
        orderOut.setOrderDesc(order.getOrderDesc());
        orderOut.setStatus(order.getStatus());
        orderOut.setCarOut(getCar(order.getCars().get(0)));
        return orderOut;
    }

    public OrderOut getOrder(Order order, CarOut carOut) {
        OrderOut orderOut = new OrderOut();
        orderOut.setId(order.getOrderId());
        orderOut.setOrderDesc(order.getOrderDesc());
        orderOut.setStatus(order.getStatus());
        orderOut.setCarOut(carOut);
        return orderOut;
    }
}