package com.ericpol.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class OrderIn {

    @NotEmpty
    private String orderDesc;

    public OrderIn() {
    }

    public OrderIn(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }
}
