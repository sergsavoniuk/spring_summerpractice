package com.ericpol.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

public class CarIn {

    @NotEmpty
    private String makes;

    @NotEmpty
    private String model;

    @NotEmpty
    private String engine;

    @Range(min = 0, max = 500000)
    private Double mileage;

    @Range(min = 1980, max = 2017)
    private Integer year;

    public CarIn() {
    }

    public CarIn(String makes, String model, String engine, Double mileage, Integer year) {
        this.makes = makes;
        this.model = model;
        this.engine = engine;
        this.mileage = mileage;
        this.year = year;
    }

    public String getMakes() {
        return makes;
    }

    public void setMakes(String makes) {
        this.makes = makes;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}