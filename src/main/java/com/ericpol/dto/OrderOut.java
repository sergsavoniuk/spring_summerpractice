package com.ericpol.dto;

public class OrderOut {

    private Long id;
    private String orderDesc;
    private String status;
    private CarOut carOut;

    public OrderOut() {
    }

    public OrderOut(Long id, String orderDesc, String status, CarOut carOut) {
        this.id = id;
        this.orderDesc = orderDesc;
        this.status = status;
        this.carOut = carOut;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CarOut getCarOut() {
        return carOut;
    }

    public void setCarOut(CarOut carOut) {
        this.carOut = carOut;
    }
}
