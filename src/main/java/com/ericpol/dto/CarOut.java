package com.ericpol.dto;

public class CarOut {

    private Long id;
    private String makes;
    private String model;
    private String engine;
    private Double mileage;
    private Integer year;

    public CarOut() {
    }

    public CarOut(Long id, String makes, String model, String engine, Double mileage, Integer year) {
        this.id = id;
        this.makes = makes;
        this.model = model;
        this.engine = engine;
        this.mileage = mileage;
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMakes() {
        return makes;
    }

    public void setMakes(String makes) {
        this.makes = makes;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}