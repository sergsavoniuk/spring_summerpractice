package com.ericpol.controller;

import com.ericpol.dto.FactoryTo;
import com.ericpol.dto.OrderIn;
import com.ericpol.dto.OrderOut;
import com.ericpol.model.Car;
import com.ericpol.model.Order;
import com.ericpol.service.interfaces.CarService;
import com.ericpol.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private CarService carService;

    @Autowired
    private FactoryTo factoryTo;

    @RequestMapping(value = "/client/cars/{id}/order/add", method = RequestMethod.GET)
    public String addOrderToCarPage(@PathVariable("id") Long carId, Model model) {
        Car car = carService.findById(carId);
        model.addAttribute("car", factoryTo.getCar(car));
        return "orderForCar";
    }

    @ResponseBody
    @RequestMapping(value = "/client/cars/{id}/order/add/perform", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public OrderOut addOrderToCar(@RequestBody @Valid OrderIn orderIn, @PathVariable("id") Long carId) {
        return orderService.save(orderIn, carId);
    }

    @RequestMapping(value = "/client/orders/all", method = RequestMethod.GET)
    public String getAllClientOrders(Model model) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<OrderOut> ordersOut = orderService.findAllUserOrders(userDetails.getUsername());
        model.addAttribute("orders", ordersOut);
        return "clientOrders";
    }

    @RequestMapping(value = "/mechanic/orders", method = RequestMethod.GET)
    public String ordersPage() {
        return "allOrders";
    }

    @RequestMapping(value = "/mechanic/orders/my", method = RequestMethod.GET)
    public String mechanicOrdersPage() {
        return "mechanicOrders";
    }

    @ResponseBody
    @RequestMapping(value = "/mechanic/orders/my/all", method = RequestMethod.GET, produces = "application/json")
    public List<OrderOut> getAllMechanicOrders() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return orderService.findAllMechanicOrders(userDetails.getUsername());
    }

    @ResponseBody
    @RequestMapping(value = "/mechanic/orders/all", method = RequestMethod.GET, produces = "application/json")
    public List<OrderOut> getAllOrders() {
        return orderService.findAllOrders();
    }

    @ResponseBody
    @RequestMapping(value = "/mechanic/orders/save", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public OrderOut addMechanicToOrder(@RequestBody Long orderId) {
        return orderService.save(orderId);
    }
}