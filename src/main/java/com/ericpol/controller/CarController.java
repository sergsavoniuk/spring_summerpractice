package com.ericpol.controller;

import com.ericpol.dto.CarIn;
import com.ericpol.dto.CarOut;
import com.ericpol.dto.FactoryTo;
import com.ericpol.service.interfaces.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(value = "/client/cars", method = RequestMethod.GET)
    public String carsPage() {
        return "cars";
    }

    @ResponseBody
    @RequestMapping(value = "/client/cars/save", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public CarOut saveCar(@RequestBody @Valid CarIn carIn) {
        return carService.save(carIn);
    }

    @ResponseBody
    @RequestMapping(value = "/client/cars/edit", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public CarOut editCar(@RequestBody @Valid CarOut car) {
        return carService.update(car);
    }

    @ResponseBody
    @RequestMapping(value = "/client/cars/delete/{id}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Long deleteCar(@PathVariable("id") Long id) {
        carService.delete(id);
        return id;
    }

    @ResponseBody
    @RequestMapping(value = "/client/cars/all", method = RequestMethod.GET, produces = "application/json")
    public List<CarOut> getAllUserCars() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return carService.findAllUserCars(userDetails.getUsername());
    }
}