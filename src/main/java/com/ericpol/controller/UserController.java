package com.ericpol.controller;

import com.ericpol.dto.UserTo;
import com.ericpol.model.User;
import com.ericpol.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller("userController")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String signInUserFrom() {
        return "login";
    }

    @RequestMapping(value = "/login-perform", method = RequestMethod.GET)
    public String signInUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<GrantedAuthority> roles = new ArrayList<>(userDetails.getAuthorities());
        if (roles.get(0).getAuthority().equals("ROLE_MECHANIC")) {
            return "redirect:/mechanic/homepage";
        } else {
            return "redirect:/client/homepage";
        }
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signUpPage() {
        return "registration";
    }

    @ResponseBody
    @RequestMapping(value = "/signup-perform", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public UserTo signUpUser(@RequestBody @Valid UserTo userTo) {
        User user = userService.save(userTo);
        return userTo;
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }
}