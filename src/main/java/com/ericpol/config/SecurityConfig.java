package com.ericpol.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth
        /*         .jdbcAuthentication().dataSource(dataSource)*/
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
           /*     .usersByUsernameQuery("select username, password from user where user.username=?")
                .authoritiesByUsernameQuery("select username, role_desc from user" +
                                            " inner join user_role on user.user_id = user_role.f_user_id" +
                                            " inner join role on user_role.f_role_id = role.role_id" +
                                            " where user.username=?");*/
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/resources/**", "/**").permitAll()
                .antMatchers("/" ,"/signup", "/signup-perform").permitAll()
                .antMatchers("/mechanic*").access("hasRole('ROLE_MECHANIC') and hasRole('ROLE_CLIENT')")
                .antMatchers("/client*").access("hasRole('ROLE_CLIENT')")
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .permitAll()
                .loginPage("/login")
                .loginProcessingUrl("/j_spring_security_check")
                .defaultSuccessUrl("/login-perform")
                .failureUrl("/login?error=true")
                .usernameParameter("j_username").passwordParameter("j_password")
                .and()
            .logout()
                .permitAll()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .and()
            .exceptionHandling().accessDeniedPage("/403")
                .and()
            .csrf().disable();
    }
}
