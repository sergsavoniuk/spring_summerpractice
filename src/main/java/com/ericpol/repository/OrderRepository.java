package com.ericpol.repository;

import com.ericpol.model.Order;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
    @Transactional(readOnly = true)
    @Query("select o from Order o where o.userOrder.username = ?1")
    List<Order> findAllUserOrders(String username);

    @Transactional(readOnly = true)
    @Query("select o from Order o where o.mechanicId = ?1")
    List<Order> findAllMechanicOrders(Long mechanicId);

    @Transactional
    @Query("select o from Order o")
    List<Order> findAllOrders();
}