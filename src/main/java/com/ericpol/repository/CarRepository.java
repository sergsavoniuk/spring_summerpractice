package com.ericpol.repository;

import com.ericpol.model.Car;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
    @Transactional(readOnly = true)
    @Query("select c from Car c where c.userCar.username = ?1")
    List<Car> findAllUserCars(String username);

    @Modifying
    @Transactional
    @Query(value="delete from Car c where c.id = ?1")
    void deleteById(Long carId);
}